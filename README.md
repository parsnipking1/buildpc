# Build PC installation

## Overview
- [Build Setup](#build-setup)
- [Download Ubuntu](#download-ubuntu)
- [Build Downloading](#build-downloading)
- [Build Execution](#build-execution)
- [Build Environment File](#build-environment-file)
- [Build Backend](#build-backend)
- [Build Frontend](#build-frontend)
- [Build Bowl Dependencies](#build-bowl-dependencies)
- [Build After Installation](#build-after-installation)
- [Mod Navigation](#mod-navigation)
- [Mod Execution](#mod-execution)
- [Mod After Installation](#mod-after-installation)

### Build Setup
1. Connect the new PC through a HDMI cable to a monitor. Connect a keyboard and mouse to the new PC also.
2. Open a firefox window and go to `www.gitlab.com`. Sign in as `parsnipking1` with the provided password.
3. You can access this README from the `build` directory.

### Download Ubuntu
1. Insert the provided USB into a port on the PC.
2. Turn the PC on by connecting a power source and repeatedly press the `Delete` button at the same time. This should take you to the blue BIOS settings.
3. From BIOS settings, go to `Save + Exit` tab and select the USB as the boot override option.
4. This will boot the PC into the Ubuntu installer. Select Install Ubuntu.
5. Complete the install questions with key answers:
    - Normal installation
    - Erase and install
    - Your name: wyma
    - PC name: wyma
    - Username: wyma
    - Password provided to you
    - Login automatically
6. Follow the instructions to restart the PC and remove installation USB.
7. Once restarted, set the `Screen Blank` to `Never` in `Power` Settings of the PC.

### Build Downloading
1. Once the machine has ubuntu 20.04 installed, make sure the PC is connected to the internet. Open a terminal and run this command toq  download the build scipt. (Note it is a Capital O rather than the number zero)
    ```
    wget -O build.sh https://gitlab.com/parsnipking1/buildpc/-/raw/main/build.sh?inline=false
    ```
---
2. Make the build script executable.
    ```
    chmod +x build.sh
    ```
---
3. Execute the build script with this command, followed by the PC password.
    ```
    ./build.sh
    ```


### Build Execution
Note: The chromium step in build execution may take a while. Please be patient.

1. During the ssh-key generation, user will need to press `enter` to each of the below prompts.

    ```
    Enter file in which to save the key (/home/wyma/.ssh/id_rsa): 
    ```
    ```
    Enter passphrase (empty for no passphrase): 
    ```

    ```
    Enter same passphrase again: 
    ```
---
2. Once the key has been generated, the user must copy the generated key within the shown bounds by right clicking and selecting `Copy` rather than using the command `^C`.

    ```
    Key pair created

    ### COPY BELOW KEY ### 
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD9rOIUlDFW8fTDUQB37ZtN5ObGVklOu0AVK9c67TdOl631nRwElz99bx1/Blt1ihpJPY++UN+YhhvdY9ZqPS7RsNHwT6IksWvm6+zxXJbWzPk/woxJ1q45p6ZyHY278negzDCFYgvRB0Ho5Ce1XV8eCoXXVeRnsXwy6fbCV0oGhoclA9owoclZn0kwmQhpEpXSLDQ0avC8T0KyPAz7706dVaUehP4adeIY32bVnjJHQaB0RSYE416F7Gncz6meB5GqNYt+gUoiLL+zxrDqqAaeZBDyFduTo2uvv3m8xuUZDhebH525I9dODpLOBCARxj3lgLNiFJbeIg5wxIOL3hroLpbalhDWaRADilm/vU/nbaGs48JaFTEMe1/HOFD+njUGRUVLFO0rwQ2ySEyfw25UX9cgGz3khz6xnU0txeQqWKvj1UkpG1i78XNrbLLsUCQ8XQ2IpQK3CoB//nq2DMPO2t2EOPtZwSjdkvLITeEqSmL5bR07SVbp9lROr94O9ek= “parsnipking1@gmail.com”
    ### END OF KEY ###

    Paste public key into github account
    User perferences > SSH Keys

    Once SSH Key saved press any key to RESUME
    ```
---
3. The copied key must be pasted into the gitlab account through the below path. **Ensure to remove the expiry date on the added key and to add a relevant title.** Press `enter` to continue.

    ```
    User preferences > SSH Keys
    ```
    ![picture alt](key.PNG  "ssh_key_image")
---

4. Input `yes` and press `enter` to continue on this command.

    ```
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    ```

### Build Environment File

1.    
    ```
    Please enter a value for USER_HOST
    ```
    For `USER_HOST`, input `192.168.250.<x>` where `<x>` is selected from the table provided for the install.

    Press `enter` to continue.

---
2. 
    ```
    Please enter a value for USER_PORT
    ```
    {ress `enter` to use the default port 42069.


---
3. 
    ```
    Please enter a value for SQLALCHEMY_DATABASE_URI [PRESS ENTER TO SKIP]
    ```
    For `SQLALCHEMY_DATABASE_URI`, press `enter` to set default value.
---
4. 
    ```
    Please enter a value for MOCK_IMAGE_DIR [PRESS ENTER TO SKIP]
    ```
    For `MOCK_IMAGE_DIR`, press `enter` to set default value.
---
5.
    ```
    Please enter a value for CAM_SETTINGS_PATH [PRESS ENTER TO SKIP]
    ```
    For `CAM_SETTINGS_PATH`, press `enter` to set default value.
---
6. 
    ```
    Please enter a value for B2_ACCOUNT_ID

    Please enter a value for B2_APP_KEY

    Please enter a value for B2_BUCKET_NAME
    ```
    For these commands, press `enter` to use the default (and correct) B2 account information.

---
7.
    ```
    Please enter a value for CLOUD_DIR_NAME
    ```
    For `CLOUD_DIR_NAME`, input `<x><y><z>` where:
    `<x><y><z>`  | Input Options
    :---: | :---:
    `<x>`  | First letter of the customer
    `<y>`  | Number of trimmer setup
    `<z>`  | head, tail, or bowl
    
    ie. For the tail PC on trimmer setup 3 at Hiams:  
        `<x>` = h  
        `<y>` = 3  
        `<z>` = tail  
        So the user would input: `h3tail`
    
    Press `enter` to continue.

---
8. 
    ```
    Please enter a value for BOWL_TESTMODE_SETTINGS

    Please enter a value for BOWL_CUTTER_EXECUTABLE
    ```
    Press `enter` for `BOWL_TESTMODE_SETTINGS` and `BOWL_CUTTER_EXECUTABLE` to set default values.
---
9. 
    ```
    Please enter a value for VUE_APP_MAX_OPERATION_IMAGES
    ```
    Press `enter` to set default value.
---
10. 
    ```
    Please enter a value for AUTH_KEY
    ```
    Press `enter` for this command and this will be inputted later.
---
11.
    ```
    Please enter a value for API_URL [PRESS ENTER TO SKIP]
    ```
    For `API_URL`, press `enter` to set default value.
---
12.
    ```
    Please enter a value for PULSE_INTERVAL
    ```
    Input `2` for `PULSE_INTERVAL`. Press `enter` to continue.
---
13.
    ```
    Please enter a value for MACHINE_TYPE
    ```
    If machine type is `Head`, press `enter` to input this value by default. Otherwise, input the machine type in **CAPITAL LETTERS** and press `enter` to continue.
---
14.
    ```
    Please enter a value for SIM
    ```
    Press `enter` to input 0 as default value, otherwise input value and press `enter` to continue.
---
15.
    ```
    Please enter a value for TIMEZONE_VARIABLE. Options can be found in wiki page 'available timezones'.
    ```
    Press `enter` to input 'Europe/London' as default value, otherwise input value **WITHOUT QUOTATIONS** and press `enter` to continue.
---
16.
    ```
    Please enter a value for SCREEN_TIMEOUT
    ```
    Press `enter` to input 10 minutes as default value, otherwise input value and press `enter` to continue.
---
17.
    ```
    Please enter a value for BACKUPSERVER_PORT
    ```
    Press `enter` to input 42070 default backup port, otherwise input different port number and press `enter` to continue.
---
18.
    ```
    Please enter a value for CAMERA_OFFSET
    ```
    Press `enter` to input 25 as default value. This can be changed later during comissioning.
---
19.
    ```
    Please enter a value for EMAIL_ADDRESS
    ```
    Press `enter` to input default address and continue.
---
20.
    ```
    Please enter a value for EMAIL_PASSWORD
    ```
    Press `enter` to input default password and continue.


### Build Backend
Note: Installing the C++ dependencies will take a while.
1. 
    ```
    Next step to run .install in vege/backend. Press any key to continue...

    ```
    Press `enter` to continue.
---
2. 
    ```
    This script will install Galaxy embedded SDK to your system
    press 'Enter' to continue...
    ```
    Press `enter`.
---
3. 
    ```
    This script will install Galaxy embedded SDK to your system
    press 'Enter' to continue...
    ```
    Press `enter`.
---
4. 
    ```
    Will install x86_64 SDK. Sure to continue? (Y/n) :
    ```
    Input `y`. Press `enter` to continue.
---
5.
    ```
    Choose language? (En/Cn) : 
    ```
    Input `En` to set the language to English. Press `enter` to continue.
---
6.
    ```
    [sudo] password for wyma:
    ```
    Input the PC password and press `enter` to continue.

### Build Frontend
1. 
    ```
    Next step to run .install in vege/frontend. Press Y to continue

    ```
    Press `Y` to continue if HEAD machine. For BOWL this is not needed but it is preferred for TAIL. ** DO NOT PRESS ENTER **

### Build Bowl Dependencies
1. 
    ```
    Do you want to install bowl dependencies (HIGHLY RECOMMENDED)? [y/n]

    ```
    Input `y` to install, this is **HIGHLY RECOMMENDED** and **REQUIRED FOR THE BOWL PC**. Press `enter` to continue.
---
2. 
    ```
    After this operation, <x> MB of additional disk space will be used. Do you want to continue? [Y/n] 

    ```
    Input `y`. Press `enter` to continue.
---

### Build After Installation
1. 
    ```
    Do you want to set the MACHINE_TYPE of the computer to <x>? [y/n]

    ```
    Input `y` to set to the machine type listed. Press `enter` to continue.
---
2. 
    When the `lightdm` manager comes up, use the arrows to select `lightdm` and press `enter` to continue.
---

3. After installation is complete, reboot the machine with the below command followed by pressing `enter`.
```
sudo reboot
```

### Mod Navigation
1. Open up a new terminal window. Navigate to the `vege` directory with the command `cd vege`.
---
2. Make sure that an ethernet connection is made from the PC to a local network in port LAN1.
---
3. Make the mod script executable.
    ```
    chmod +x mod.sh
    ```
---
4. Execute the build script with this command.
    ```
    ./mod.sh
    ```
---

### Mod Execution
1. 
    ```
    Do you want to overwrite it?
    ```
    Check that the .env file looks alright. If it is, enter `n`, otherwise to overwrite it enter `y`. Press `enter` to continue.
---
2. 
    This is the start of the ***NETWORKING SECTION***.
    ```
    Do you want to skip the network configuration? (yes/no)
    ```
    Input `no` and then press `enter` to continue.
---
3. 
    ```
    Do you want to set USER HOST to <x>? [y/n]
    ```
    Check the address is what you expected, then input `y` and then press `enter` to continue.
---
4. 
    The following information will come up.
    ```
    Current network interfaces and their IP addresses:
    lo               UNKNOWN        ADDRESS1
    enp1s0           UP             ADDRESS2
    enp2s0           DOWN
    wlp3s0           UP             ADDRESS3
    Available interfaces:
        1  lo
        2  enp1s0
        3  enp2s0
        4  wlp3s0
    Enter the number for the STATIC IP interface?
    ```
    Note: The number after enp<x>s0 may be different to this example.
    The static interface needs to be set as the enp network that has a connected IP ADDRESS, ie. LAN1. In this instance, input `2` (as enp1s0 on the list is #2) and then press `enter` to continue.
---
5. 
    ```
    Enter the number for the DHCP interface: 
    ```
    Select the other enp interface for the DHCP interface. In this instance, input `3` (as enp2s0 on the list is #3) and then press `enter` to continue.
---
6. 
    ```
    Enter Gateway for enp1s0 (press enter to skip):
    ```
    Input `192.168.250.200` and then press `enter` to continue.
---
7. 
    ```
    Enter DNS servers for enp1s0 separated by a comma (press enter to skip): 
    ```
    Input `1.1.1.1`. Press `enter` to continue.
---
8. 
    ```
    Do you want to apply this configuration? (y/n):
    ```
    Check the configuration is as expected. Input `y`, then then press `enter`. Input the PC password to continue.
---
9. 
    This is the start of the ***HOSTNAME SECTION***.
    ```
    Do you want to skip hostname configuration? (yes/no):
    ```
    Input `no` then press `enter` to continue.
---
10. 
    ```
    Do you want to change the hostname to <x>: [y/n]
    ```
    Check hostname is as expected. Input `y` then press `enter`. Input the PC password to continue.
---
11. 
    This is the start of the ***SERVICES SECTION***. 
    ```
    Do you want to skip service configuration? (yes/no): 
    ```
    Input `no` then press `enter` to continue.
---
12. 
    ```
    Do you want to set the MACHINE_TYPE of the computer to HEAD? [y/n]
    ```
    Check hostname is as expected. Input `y` then press `enter` to continue.
---
13. Enter the PC password when prompted and press `enter` to continue.


### Mod After Installation
After installation is complete, reboot the machine with the below command followed by pressing `enter`.
```
sudo reboot
```

### BIOS setup
1. After the previous reboot, enter the BIOS settings as described previously.
2. Move to (on original PCs):
    ``` Chipset > PCH-IO Configuration > State After G3     ```
    Set this value to `S0 State`
