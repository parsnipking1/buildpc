from PIL import Image, ImageTk
import tkinter as tk
from tkinter import ttk

# Create a blank window
window = tk.Tk()
window.attributes('-fullscreen', True)

image = Image.open('wyma_wallpaper.png')

screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()
image = image.resize((screen_width, screen_height))

tk_image = ImageTk.PhotoImage(image)

label = tk.Label(window, image=tk_image)
label.place(x=0, y=0, relwidth=1, relheight=1)

# Create a progress bar
style = ttk.Style()
style.configure("Custom.Horizontal.TProgressbar", troughcolor='white', background='#00b4e4')

progress_bar = ttk.Progressbar(window, style="Custom.Horizontal.TProgressbar", mode='determinate', maximum=20)
progress_bar.place(x=0, y=window.winfo_screenheight() - 20, relwidth=1)

def update_progress():
    progress_bar['value'] += 1
    if progress_bar['value'] < 20:
        window.after(1000, update_progress)

# Update the progress bar every second
window.after(1000, update_progress)

window.mainloop()
