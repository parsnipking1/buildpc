# RUNNING OUT OF TIME SO BELOW ARE THE STEPS TO APPLY THE WYMA SPLASH_SCREEN

# PULL CONTENT FROM SPLASH_SCREEN FOLDER TO HOME
# WHEN INSIDE DIR
```
rsync /home/wyma/splash_screen/*  /home/wyma/ --exclude="README.md" --exclude='.desktop'
```

# MOVE WYMA_SCREEN DESKTOP FILE
```
cp wyma_screen.desktop /home/wyma/.config/autostart/wyma_screen.desktop
```
