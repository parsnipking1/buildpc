#!/bin/bash

echo "Loading environment variables from vege/.env"
source /home/wyma/vege/.env

# Specifies the display top be manipulated
export DISPLAY=:0

# LAUNCH CHROME IN KIOSK MODE
chromium-browser --kiosk $USER_HOST:8080 --no-first-run --touch-events=enabled --fast --fast-start --disable-popup-blocking --disable-infobars --disable-session-crashed-bubble --disable-tab-switcher --disable-translate --enable-low-res-tiling
#chromium-browser --noerrors --disable-session-crashed-bubble --disable-infobars --kiosk $USER_HOST:8080
