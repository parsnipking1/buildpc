#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
# ############################################################
# # MAIN                                                     #
# ############################################################
        
sudo apt-get update  # To get the latest package lists
sudo apt upgrade -y

# REMOVE UPDATE MANAGER - REMOVE POPUPS
sudo apt-get remove update-manager -y

# VIM
sudo apt install vim -y

# CHROMIUM BROWSER
sudo snap install chromium
sudo apt-get install chromium-browser -y

# SCREEN and PYFLAKES
sudo apt install screen -y
sudo apt install pyflakes3 -y

# RCLONE
sudo apt-get install rclone -y

# SSH
sudo apt-get install openssh-server -y
sudo apt install sshpass -y

# CURL
sudo apt-get install curl -y

# VNC
sudo apt-get install x11vnc -y
mkdir ~/.vnc    # directory for password storage
x11vnc -storepasswd parsnip ~/.vnc/x11pass.vnc

# LIGHTDM - FOR KIOSK - REMOVED PROMPTS CONFIGURE LATER
sudo -E apt-get install lightdm -y

# GIT
sudo apt-get install git -y

# INSTALL PROTOBUF COMPILER
sudo apt install -y protobuf-compiler

# CHECK AND CREATE KEY PAIR
if [ -f ~/.ssh/id_rsa.pub ];
then
    echo "Public key exists"
else
    echo "No public key exists - creating key"
    echo
    echo "### Creating SSH KEY PAIR ### "
    echo
    echo "Press enter to following prompts to resume key generation"
    echo
    ssh-keygen -o -t rsa -C “parsnipking1@gmail.com”
    echo "Key pair created"
    echo
fi

echo "### COPY BELOW KEY ### "
cat ~/.ssh/id_rsa.pub
echo "### END OF KEY ###"
echo
echo "Paste public key into github account"
echo "User perferences > SSH Keys"
echo
read -p "Once SSH Key saved press any key to RESUME"
echo "### BUILD RESUME ###"

if [ -d ~/vege ]; 
then
    echo "The vege repo is already cloned"
else
    echo "Cloning vege repo..."
    git clone git@gitlab.com:rnd-vision/vege.git
    git config --global user.email "parsnipking1@gmail.com"
    git config --global user.name "Par Snip"
fi

if [ -d ~/c_vege ]; 
then
    echo "The c_vege repo is already cloned"
else
    echo "Cloning c_vege repo..."
    git clone git@gitlab.com:mitch-cmd/c_vege.git
    git config --global user.email "parsnipking1@gmail.com"
    git config --global user.name "Par Snip"
fi

# GIT CHECKOUT CORRECT BRANCH
# Function to checkout the origin/master branch for a given repository
checkout_origin_master() {
    local repo_dir=$1
    echo
    echo "Checking for local changes in the repository at ${repo_dir}"

    # Navigate to the repository's working directory
    cd $HOME/$repo_dir

    # Check if there are any unstaged or uncommitted changes
    if git -C "$HOME/$repo_dir" diff --quiet HEAD --; then
        echo "No local changes detected in ${repo_dir}. Proceeding with checkout."
    else
        # Prompt the user for confirmation
        echo "Warning: Local changes detected in ${repo_dir}. Continuing will discard these changes."
        read -p "Are you sure you want to continue? (y/n) " -n 1 -r
        echo    # Move to a new line
        if [[ $REPLY =~ ^[Yy]$ ]]; then
            echo "Proceeding with checkout in ${repo_dir}..."
        else
            echo "Checkout canceled for ${repo_dir}. Your local changes are safe."
            return 1
        fi
    fi

    # Checkout the master branch and pull remote changes
    git --git-dir $HOME/${repo_dir}/.git --work-tree $HOME/${repo_dir} fetch origin
    git --git-dir $HOME/${repo_dir}/.git --work-tree $HOME/${repo_dir} checkout master --force
    git --git-dir $HOME/${repo_dir}/.git --work-tree $HOME/${repo_dir} pull

    echo "Checkout to origin/master successful for ${repo_dir}."
    echo
}

# Path to the repositories
repo_paths=("/vege" "/c_vege")

# Iterate through each repository and attempt to checkout origin/master
for repo_path in "${repo_paths[@]}"; do
    checkout_origin_master $repo_path || { echo "An error occurred with ${repo_path}. Stopping script."; exit 1; }
done
echo
echo "All repositories updated successfully."
echo
# SET ENV VARIABLES
cd ~/vege
chmod +x set-env.sh
./set-env.sh

# Check the exit code of set-env.sh
exit_code=$?

if [[ $exit_code -eq 1 ]]; then
    echo "set-env.sh exited early due to the condition being met. Continuing with build.sh."
elif [[ $exit_code -eq 0 ]]; then
    echo "set-env.sh completed successfully. Continuing with build.sh."
else
    echo "set-env.sh encountered an error. Exiting build.sh."
    exit $exit_code
fi

# SET RCLONE CONFIG FOR WRITING TO B2BLAZE

# Check if rclone is installed
if ! [ -x "$(command -v rclone)" ]; then
  # Install rclone if it is not installed
  echo "rclone not found - installing package"
  sudo apt-get install rclone -y
fi

# Set up the B2 remote in rclone
source /home/wyma/vege/frontend/.env
rclone config create bb b2 account $B2_ACCOUNT_ID key $B2_APP_KEY

echo "Next step to run ./install in vege/backend"
echo "Press any key to continue or CTRL+C to exit..."
read -n 1 -s -r -p "Press any key to continue"
echo
# INSTALL BACKEND
cd ~/vege/backend
git fetch origin
./install.sh

echo
echo "Next step to run ./install in vege/frontend"
echo "Would you like to proceed with the installation or skip this step?"
echo "Press 'Y' to continue with the installation, or 'N' to skip it."

# Wait for user input: Y to proceed, N to skip
read -n 1 -s -r -p "(Y/N): " user_input
echo

case "$user_input" in
    [Yy])
        echo "Proceeding with installation..."
        cd ~/vege/frontend && chmod +x install.sh && ./install.sh
        ;;
    *)
        echo "Skipping installation..."
        ;;
esac

echo
echo "Would you like to install bowl dependencies?"
echo "Press 'Y' to continue with the installation, or 'N' to skip it."

# Wait for user input: Y to proceed, N to skip
read -n 1 -s -r -p "(Y/N): " user_input
echo

case "$user_input" in
    [Yy])
        echo "Proceeding with installation..."
        cd ~/c_vege && chmod +x install.sh && ./install.sh
        ;;
    *)
        echo "Skipping installation..."
        ;;
esac

# SETUP CRON JOB
cd ~/vege/cron
./set_job.sh

# WRITE SET_SERVICES TO SUDOERS FILE TO EXCLUDE NEED TO TYPE PASSWORD 6 TIMES!!
echo "wyma ALL=(ALL) NOPASSWD: /home/wyma/vege/services/set_services.sh to /etc/sudoers file"        
sudo sh -c 'echo "wyma ALL=(ALL) NOPASSWD: /home/wyma/vege/services/set_services.sh" >> /etc/sudoers'
sudo sh -c 'echo "wyma ALL=(ALL) NOPASSWD: /bin/systemctl restart backend.service" >> /etc/sudoers'
sudo sh -c 'echo "wyma ALL=(ALL) NOPASSWD: /bin/systemctl restart frontend.service" >> /etc/sudoers'
sudo sh -c 'echo "wyma ALL=(ALL) NOPASSWD: /bin/systemctl restart vnc.service" >> /etc/sudoers'
sudo sh -c 'echo "wyma ALL=(ALL) NOPASSWD: /bin/systemctl restart _backup_backend.service" >> /etc/sudoers'

# SETUP SERVICES
cd ~/vege/services
sudo ./set_services.sh

# KIOSK AND AUTO LOGIN
cp -r /home/wyma/vege/kiosk/*.sh /home/wyma/
cd /home/wyma/
chmod +x kiosk.sh

# EDIT GIT HOOKS TO RECORD COMMITS AND CHANGING BRANCHES
cd /home/wyma/vege/scripts
chmod +x git_hooks.sh

# DONT RUN ./KIOSK AS MACHINE COULD BE TAIL OR BOWL YET
# echo "Run kiosk.sh..."
# ./kiosk.sh

# AT END OF BUILD CONFIGURE THE LIGHTDM
sudo dpkg-reconfigure lightdm
echo
echo "SUDO REBOOT THIS MACHINE"

unset DEBIAN_FRONTEND
