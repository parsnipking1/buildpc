echo "Installing bowl dependencies..."
sudo apt install build-essential cmake git pkg-config libgtk-3-dev libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libjpeg-dev libpng-dev libtiff-dev gfortran openexr libatlas-base-dev python3-dev python3-numpy libtbb2 libtbb-dev libdc1394-22-dev libopenexr-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev libjsoncpp-dev libopencv-videoio-dev

sudo apt-get install libprotobuf-dev protobuf-compiler

echo "Making opencv directory"
mkdir ~/opencv_build && cd ~/opencv_build
echo "git clone https://github.com/opencv/opencv.git"
git clone https://github.com/opencv/opencv.git
echo "git clone https://github.com/opencv/opencv_contrib.git"
git clone https://github.com/opencv/opencv_contrib.git

echo "Making build directory"
cd ~/opencv_build/opencv
mkdir -p build && cd build

echo "Setting up CMake"
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D OPENCV_GENERATE_PKGCONFIG=ON -D OPENCV_EXTRA_MODULES_PATH=~/opencv_build/opencv_contrib/modules -D BUILD_EXAMPLES=ON ..

echo "Compiling bowl dependencies, this will likely take a while..."
make -j8

echo "Installing openCV"
sudo make install
echo "OpenCV installed"

cd
echo "git clone https://github.com/nadjieb/cpp-mjpeg-streamer.git"
if [ -d ~/cpp-mjpeg-streamer ]; 
then
    echo "The cpp-mjpeg-streamer repo is already cloned"
    echo "Making build directory"
    cd cpp-mjpeg-streamer/build
    cmake .. && make
else
    echo "Cloning cpp-mjpeg-streamer repo..."
    git clone https://github.com/nadjieb/cpp-mjpeg-streamer.git
    echo "Making build directory"
    cd cpp-mjpeg-streamer
    mkdir build && cd build
    cmake .. && make
fi

echo "Installing mjpeg-streamer"
sudo make install
