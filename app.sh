#!/bin/bash

echo "Loading environment variables from vege/.env"
source /home/wyma/vege/.env

# Specifies the display top be manipulated
export DISPLAY=:0

# LAUNCH FIREFOX IN KIOSK MODE
chromium-browser --noerrors --disable-session-crashed-bubble --disable-infobars --kiosk $USER_HOST:8080
#/usr/bin/firefox --kiosk $USER_HOST:8080
