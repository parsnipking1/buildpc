#!/bin/bash

# Download and install Chromium browser
sudo apt-get update
sudo apt-get install chromium-browser -y

# Check if the line exists
if grep -q "# LAUNCH FIREFOX IN KIOSK MODE" app.sh; then
    # Delete the line
    sed -i '/# LAUNCH FIREFOX IN KIOSK MODE/d' app.sh

    # Insert new line
    sed -i '9i # LAUNCH CHROME IN KIOSK MODE\nchromium-browser --noerrors --disable-session-crashed-bubble --disable-infobars --kiosk $USER_HOST:8080' app.sh

    echo "Replaced the line with the Chromium browser command."
else
    echo "The line '# LAUNCH FIREFOX IN KIOSK MODE' does not exist in app.sh. No changes were made."
fi
