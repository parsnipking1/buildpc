echo "Setting autostart for kiosk app (user interface)"
echo "Setting auto-login in /etc/lightdm/lightdm.conf"
echo
sudo bash -c "echo '[SeatDefaults]
autologin-user=wyma
autologin-user-timeout=0
user-session=ubuntu
greeter-session=unity-greeter' > /etc/lightdm/lightdm.conf"
echo
echo "Set user in /etc/lightdm/lightdm.conf.d/50-myconfig.conf"
sudo bash -c "echo '[SeatDefaults]
autologin-user=wyma' > /etc/lightdm/lightdm.conf.d/50-myconfig.conf"
echo
echo "Setup autostart to launch kiosk"
sudo mkdir /home/wyma/.config/autostart
echo "Editing /home/wyma/.config/autostart/wyma.desktop"
sudo bash -c "echo '[Desktop Entry]
Type=Application
Name=Wyma Kiosk
Exec=/home/wyma/app.sh
X-GNOME-Autostart-enabled=true' > /home/wyma/.config/autostart/wyma.desktop"
echo 
echo "Make app.sh executable"
chmod +x home/wyma/app.sh
echo "Disabling touch gestures to stop user exiting"
cd /tmp
wget https://extensions.gnome.org/extension-data/disable-gestures-2021verycrazydog.gmail.com.v4.shell-extension.zip
echo "Install gnome-extension package"
gnome-extensions install disable-gestures-2021verycrazydog.gmail.com.v4.shell-extension.zip
echo "Enable gnome-extension package"
gnome-extensions enable disable-gestures-2021verycrazydog.gmail.com.v4.shell-extension
echo "Machine will require reboot to apply touch gesture setting"
